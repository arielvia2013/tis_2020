<html>
<?php
include("conex.php");
$link=conectar();
mysqli_set_charset($link,'utf8');

?>
<head>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.min.css" rel="stylesheet">
     <script src="dist/js/jquery-3.4.1.min.js"></script>
     <script src="dist/js/jquery-3.1.1.min.js"></script>
     <script src="dist/js/bootstrap.min.js"></script>
     <script src="dist/js/bootstrap.bundle.min.js"></script>
     <script src="dist/js/bootstrap.bundle.js"></script>
     <script src="dist/js/bootstrap.js"></script>
     <!---fontawesome 5--->
     <script src="dist/js/all.js"></script>
     <!---------->

</head>
<body>
<h1>LISTADO DE POSTULANTES</h1>
<div class="col-md-12">
       <table class="table table-bordered">
          <thead>
            <tr>
               <!---<th>CODIGO</th>--->
               <th>NOMBRE DE POSTULANTE</th>
               <th>APELLIDO PATERNO</th>
               <th>APELLIDO MATERNO</th>
               <th>CARNET DE IDENTIDAD</th>
               <th>Documentos presentados</th> 
                
               
         
                           
            </tr>          
          </thead> 

          <tbody>
          <?php 
          if(isset($_GET['ID'])){
            $id= $_GET['ID'];
            $query = "SELECT NOM_POSTULANTE, APELLIDO_P, APELLIDO_M, CI_HABILITADO, FECHA_HABILITADO, FECHA_RESULTADO, NOM_CONVOCATORIA 
			FROM postulante, habilitado, convocatoria 
			WHERE habilitado.COD_CONVOCATORIA= convocatoria.COD_CONVOCATORIA AND 
			habilitado.CI_HABILITADO= postulante.CI_POSTULANTE AND 
			convocatoria.COD_CONVOCATORIA= '$id' ";
            $resultado_conv = mysqli_query($link,$query);
            while($row = mysqli_fetch_array($resultado_conv)){ ?>
       
              <tr>
                <td><?php echo $row['NOM_POSTULANTE']?></td>
                <td><?php echo $row['APELLIDO_P']?></td>
                <td><?php echo $row['APELLIDO_M']?></td>
                <td><?php  echo $row['CI_HABILITADO']?></td>
                <td>
                 <?php $nom_convocatoria= $row['NOM_CONVOCATORIA'];?>
                 <a href="listar_arch1.php?nom_convocatoria=<?php echo $nom_convocatoria;?>&nom_pos=<?php echo $row['NOM_POSTULANTE'];?>&ape_pos=<?php echo $row['APELLIDO_P'];?>" class="btn btn-danger">
                 <i class= "far fa-trash-alt"></i>Ver Documentos presentados
                 </a>
                </td>
                
                
                
              </tr>

          <?php  }  } ?>
          
          </tbody>
       
       </table>
       
       </div>

   
    <!---<table border="1">
    <tr>
    <th>CODIGO</th>
    <th>NOMBRE CONVOCATORIA</th>
	<th>MATERIA</th>
	<th>DEPARTAMENTO</th>
    <th>FECHA INICIO</th>
    <th>FECHA FIN</th>
    </tr>
    
    </table>--->
    

</body>
</html>