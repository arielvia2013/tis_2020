<?php

     include("conex.php");
     $link=conectar();
	 $periodo=$_REQUEST['cbx_periodo'];
	 /*$consulta="SELECT facultad.NOM_FACULTAD,departamento.NOM_DEPARTAMENTO,convocatoria.NOM_CONVOCATORIA, materia.NOM_MATERIA,
	                 convocatoria.PERIODO,convocatoria.FECHA_RESULTADO,convocatoria.cod_convocatoria
	            FROM convocatoria, materia, departamento, facultad
                WHERE convocatoria.ID_MATERIA=materia.ID_MATERIA and 
				      materia.ID_DEPARTAMENTO=departamento.ID_DEPARTAMENTO and              
					  departamento.ID_FACULTAD=facultad.ID_FACULTAD
			    ORDER BY facultad.ID_FACULTAD";*/
	$consulta="SELECT departamento.NOM_DEPARTAMENTO,facultad.NOM_FACULTAD,convocatoria.NOM_CONVOCATORIA, materia.NOM_MATERIA,
	                   convocatoria.FECHA_RESULTADO,convocatoria.cod_convocatoria,convocatoria.PERIODO
	            FROM convocatoria, materia, departamento, facultad
                WHERE convocatoria.ID_MATERIA=materia.ID_MATERIA and 
				      materia.ID_DEPARTAMENTO=departamento.ID_DEPARTAMENTO and              
					  departamento.ID_FACULTAD=facultad.ID_FACULTAD and
					  convocatoria.PERIODO='$periodo'
			    ORDER BY convocatoria.PERIODO";
     $resultado=mysqli_query($link,$consulta) ;

?>
<html lang="es">
   <head>
   
       <meta name="viewport" content="width=device-width, initial-scale=1">
	   <link href="dist/css/bootstrap.min.css" rel="stylesheet">
	   <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
	   <link href="dist/css/bootstrap-reboot.css" rel="stylesheet">
	   <link href="dist/css/bootstrap-grid.min.css" rel="stylesheet">
	   <link href="dist/css/bootstrap-reboot.min.css" rel="stylesheet">
	   <script src="dist/js/jquery-3.4.1.min.js"></script>
	   <script src="dist/js/jquery-3.1.1.min.js"></script>
	   <script src="dist/js/bootstrap.min.js"></script>
       <script src="dist/js/bootstrap.bundle.min.js"></script>
       <script src="dist/js/bootstrap.bundle.js"></script>
	   <script src="dist/js/bootstrap.js"></script>
	   
   </head>
   
   <body>
  
      <br>
	 
      <div class="container">
      
	     
	    
	     <div class="row">
	       <a href="listar_convocatorias.php" class="btn btn-primary">VOLVER</a>
		 </div>
		 <br>
		 <!--div> 
		   <form action="<?php $_SERVER['PHP_SELF']; ?>" method="POST">
		       <b>Nombre: </b><input type="text" id="campo" name="campo"/>
			   <input type="submit" id="enviar" name="enviar" value="Buscar" class="btn btn-info" />
		   </form>
	     </div-->
		 
		 <br>
		 <h4>LISTADO DE CONVOCATORIAS POR PERIODO</h4>
		 <div class="row table-responsive">
		    <table class="table table-striped">
			<thead>
			<tr>
			<th>PERIODO</th>
			<th>MATERIA</th>
			<th>CONVOCATORIA</th>
			<th>DEPARTAMENTO</th>
			<th>FACULTAD</th>
			<th>FECHA RESULTADO</th>
			<th></th>
			<th></th>
			</tr>
			</thead>
			<tbody>
			   <?php while($row=$resultado->fetch_array(MYSQL_ASSOC)) { ?>
			   <tr>
			       <td><?php echo $row['PERIODO']; ?></td>
			       <td><?php echo $row['NOM_MATERIA']; ?></td>
				   <td><?php echo $row['NOM_CONVOCATORIA']; ?></td>
				   <td><?php echo $row['NOM_DEPARTAMENTO']; ?></td>
				   <td><?php echo $row['NOM_FACULTAD']; ?></td>
				   <td><?php echo $row['FECHA_RESULTADO']; ?></td>
				   <!--td><a href="modifica.php?id=<?php echo $row['id'];?>">
				   <button class="btn btn-success">Modificar</button></a></td>
				   <td><a href="#" data-href="eliminar.php?id=<?php echo $row['id'];?>" data-toggle="modal"
				   data-target="#confirm-delete">
				   <button class="btn btn-danger">Eliminar</button></a></td-->
				   <td><a href="convocatoria_detalles.php?cod_convocatoria=<?php echo $row['cod_convocatoria'];?>";>
				   <button class="btn btn-info">INFORMACION</button></a>
				   </td>
			   </tr>
			   <?php } ?>
			</tbody>
		    </table>
		 </div>
		 
      </div>
	  
	  <!--Modal-->
	  <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog"
	  aria-labelledby="myModalLabel" aria-hidden="true">
	     <div class="modal-dialog">
		     <div class="modal-content">
			 
			     <div class="modal-header">
				     <button type="button" class="close" data-dismiss="modal"
					   aria-hidden="true">&times;</button>
					 <h4 class="modal-title" id="muModalLabel">Eliminar Registro</h4>
				 </div>
				 
                 <div class="modal-body">
                     Â¿Desea eliminar este registro?
                 </div>
				 
				 <div class="modal-footer">
				      <button type="button" class="btn btn-default"
					   data-dismiss="modal">Cancel</button>
					  <a class="btn btn-danger btn-ok">Delete</a>
				 </div>
		     </div>
	       </div>
     </div>
	 
	 <script>
	    $('#confirm-delete').on('show.bs.modal', function(e){
			$(this).find('.btn-ok').attr('href',$(e.relatedTarget).data('href'));
			
			$('.debug-url').html('Delete URL: <strong>' + $(this).find(
			'.btn-ok').attr('href')+'</strong>');
		});
	 </script>
	 
   </body>
   
 </html>