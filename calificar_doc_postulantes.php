<html>
<head>
     <script language="javascript" src="js/jquery-3.4.1.min.js"></script>
     <script src="js/jquery-3.4.1.min.js"></script>
     <script src="js/jquery-3.1.1.min.js"></script>
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	 <link href="dist/css/bootstrap.min.css" rel="stylesheet">
	 <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
	 <script src="dist/js/jquery-3.4.1.min.js"></script>
	 <script src="dist/js/bootstrap.min.js"></script>
</head>
<?php
include("conex.php");
$link=conectar();
mysqli_set_charset($link,'utf8');
$id_convocatoria=$_REQUEST['cbx_convocatoria'];

$sql="SELECT *
       FROM documentacion_meritos
	   WHERE ID_CONVOCATORIA='$id_convocatoria' AND
	         HABILITADO='1'";
$res=mysqli_query($link,$sql);
while($row=mysqli_fetch_array($res))
{
	$id_seccion=$row['ID_SECCION'];
	$id_subseccion=$row['ID_SUBSECCION'];
	$id_doc=$row['ID_DOC'];
	$sql1="SELECT MAX(cantidad) as maximo
	       FROM doc_postulante
		   WHERE ID_CONVOCATORIA='$id_convocatoria' AND
		         ID_SECCION='$id_seccion' AND
				 ID_SUBSECCION='$id_subseccion' AND
				 ID_DOC='$id_doc'";
	$res1=mysqli_query($link,$sql1);
	$row1=mysqli_fetch_array($res1);
	$max=$row1['maximo'];
	unset ($sql1,$res1,$row1);//clave
	
	$sql2="INSERT INTO records(ID_CONVOCATORIA,ID_SECCION,ID_SUBSECCION,ID_DOC,CANTIDAD_RECORD)
	              VALUES('$id_convocatoria','$id_seccion','$id_subseccion','$id_doc','$max')";
	$res2=mysqli_query($link,$sql2);
	unset($sql2,$res2);
	
}
unset($sql,$res,$row);
//2da parte
$sql80="SELECT ID_POSTULANTE
        FROM inscripcion
		WHERE ID_CONVOCATORIA='$id_convocatoria'";
$res80=mysqli_query($link,$sql80);
while($row80=mysqli_fetch_array($res80))
{
  $id_postulante=$row80['ID_POSTULANTE'];
  $sql90="SELECT ID_POSTULANTE,ID_SECCION, ID_SUBSECCION, ID_DOC, CANTIDAD, PUNTAJE_SISTEMA,PUNTAJE_MAXIMO
          FROM doc_postulante
		  WHERE ID_CONVOCATORIA='$id_convocatoria' AND
		        ID_POSTULANTE='$id_postulante'";
  $res90=mysqli_query($link,$sql90);
  while($row90=mysqli_fetch_array($res90))
  {
     $id_seccion_aux=$row90['ID_SECCION'];
	 $id_subseccion_aux=$row90['ID_SUBSECCION'];
	 $id_doc_aux=$row90['ID_DOC'];
	 $puntaje_maximo=$row90['PUNTAJE_MAXIMO'];
	 $id_postulante_aux=$row90['ID_POSTULANTE'];
	 $sql100="SELECT CANTIDAD_RECORD
	          FROM records
			  WHERE ID_CONVOCATORIA='$id_convocatoria' AND
			        ID_SECCION='$id_seccion_aux' AND
					ID_SUBSECCION='$id_subseccion_aux' AND
					ID_DOC='$id_doc_aux'";
	 $res100=mysqli_query($link,$sql100);
	 $row100=mysqli_fetch_array($res100);
	 $cantidad_record=$row100['CANTIDAD_RECORD'];
	 $cantidad_postulante=$row90['CANTIDAD'];
     //$resultado=($cantidad_postulante)*100;
     //$resultado2=0;
	 //echo 	 $cantidad_record;
	 //echo 	 $cantidad_postulante;
	 if($cantidad_record>0)
	 {
		 $res300=$cantidad_postulante/$cantidad_record;
		 $resuelto=$res300*$puntaje_maximo;
	 }
     else
	 {
		 $resuelto=0;
	 }
	 //echo   $id_postulante;
     //echo 	 $cantidad_record;
	 //echo 	 $cantidad_postulante;
	 //echo 	 $resuelto;
     $sql200="UPDATE doc_postulante
              SET PUNTAJE_SISTEMA='$resuelto'
              WHERE ID_CONVOCATORIA='$id_convocatoria' AND
			        ID_SECCION='$id_seccion_aux' AND
					ID_SUBSECCION='$id_subseccion_aux' AND
					ID_DOC='$id_doc_aux' AND
					ID_POSTULANTE='$id_postulante_aux'";
     $res200=mysqli_query($link, $sql200);					
     $auxiliar=true;
	 if(!$res200)
	 {
		$auxiliar=false; 
	 }
	 unset ($sql200,$res200,$sql100,$res100,$row100,$cantidad_record,$resultado2);

  }	  
	
  unset ($sql90,$res90,$row90);	
}
unset ($sql80,$res80,$row80);
$sql6000="UPDATE convocatoria
          SET CALIFICADA='1'
		  WHERE COD_CONVOCATORIA='$id_convocatoria'";
$res6000=mysqli_query($link,$sql6000);
if($auxiliar)
{ ?>
	<div class="container">
	   <div class="row">
	    <h3>Se califico correctamente</h3>
		<a href="califica_conv_sistema.php?id=<?php echo $id_convocatoria ?>" class="btn btn-success">Continuar</a
	   </div>
	</div>
  <?php	
}
else
{ 
  ?>
	<div class="container">
	   <div class="row">
	    <h3>Error en la calificacion</h3>
		<a href="califica_conv_sistema.php?id=<?php echo $id_convocatoria ?>" class="btn btn-success">Continuar</a
	   </div>
	</div>
  <?php	

}	
 
?>
</html>