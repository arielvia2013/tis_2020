<?php
  include("conex.php");
  $link=conectar();
  $consulta="SELECT ID_FACULTAD,NOM_FACULTAD
             FROM facultad";
  $res1=mysqli_query($link,$consulta);
  
  $consulta2="SELECT ID_DEPARTAMENTO,NOM_DEPARTAMENTO
             FROM departamento";
  $res2=mysqli_query($link,$consulta2);
  
  $consulta3="SELECT ID_MATERIA,NOM_MATERIA
             FROM materia";
  $res3=mysqli_query($link,$consulta3); 

  $consulta4="SELECT DISTINCT PERIODO
             FROM convocatoria";
  $res4=mysqli_query($link,$consulta4);   
 ?>
<html>
  <head>
     <title> Listado de convocatorias </title>
     <script language="javascript" src="js/jquery-3.4.1.min.js"></script>
     <script src="js/jquery-3.4.1.min.js"></script>
     <script src="js/jquery-3.1.1.min.js"></script>
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	 <link href="dist/css/bootstrap.min.css" rel="stylesheet">
	 <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
	 <script src="dist/js/jquery-3.4.1.min.js"></script>
	 <script src="dist/js/bootstrap.min.js"></script>
     <!--script language="javascript">
        $(document).ready(function(){
          $("#cbx_facultad").change(function(){

              $('#cbx_materia').find('option').remove().end().append(
              '<option value="whatever"></option>').val('whatever');

              $("#cbx_facultad option:selected").each(function(){
                 id_facultad=$(this).val();
                 $.post("includes/getDepartamento.php", { id_facultad: id_facultad}, function(data){
                          $("#cbx_departamento").html(data);
                 });

              });

          })
        });

         $(document).ready(function(){
          $("#cbx_departamento").change(function(){

              $("#cbx_departamento option:selected").each(function(){
                 id_departamento=$(this).val();
                 $.post("includes/getMateria.php", { id_departamento: id_departamento}, function(data){
                          $("#cbx_materia").html(data);
                 });

              });

          })
        });
     </script-->
  </head>
  
<body>

    <form id="combo" name="combo" action="facu2.php" method="POST">
	<!--div style="width:800px; margin-left:auto; margin-right:auto;
    background-color:#78909c; background-size: cover; padding:8px;"-->
    <div style="width:800px; margin-left:auto; margin-right:auto;
    background-color:#b2dfdb; background-size: cover; padding:8px;">
	<h4 align=center><font color="#000000">LISTADO DE CONVOCATORIAS</font></h4><BR>
    <table aling="center" width="780" cellspacing="2" cellpadding="2" border="0">
    <tr>
    <td colspan="2" align="center" bgcolor=#4caf50><font face="verdana"color="white">CONVOCATORIAS POR FACULTAD</font> </td>
    </tr>
    <tr>
    <tr>
    <td aling="right"> Selecciona Facultad: </td>
    <td><select id="cbx_facultad" name="cbx_facultad">
        <option value="0">Seleccionar Facultad</option>
        <?php while($datos1=mysqli_fetch_array($res1)) { ?>
          <option value="<?php echo $datos1['ID_FACULTAD']; ?>"><?php echo $datos1['NOM_FACULTAD']; ?> </option>
        <?php } ?> 
        </select>
    </td>
    </tr>
    </table>
	<td colspan="2" align="center"><button type="submit" class="btn btn-success" >Buscar</button></td>
	<br><br><br>
    </form>
	
	<form id="combo" name="combo" action="depa2.php" method="POST">
	<!--div style="width:800px; margin-left:auto; margin-right:auto;
    background-color:#78909c; background-size: cover; padding:8px;"-->
    <table aling="center" width="780" cellspacing="2" cellpadding="2" border="0">
    <tr>
    <!--td colspan="2" align="center" bgcolor=#1e88e5><font face="verdana"color="white">CONVOCATORIAS POR DEPARTAMENTO</font> </td-->
    <td colspan="2" align="center" bgcolor=#00acc1><font face="verdana"color="white">CONVOCATORIAS POR DEPARTAMENTO</font> </td>
	</tr>
    <tr>
    <td aling="right"> Selecciona Departamento: </td>
    <td><select id="cbx_departamento" name="cbx_departamento">
        <option value="0">Seleccionar Departamento</option>
        <?php while($datos2=mysqli_fetch_array($res2)) { ?>
          <option value="<?php echo $datos2['ID_DEPARTAMENTO']; ?>"><?php echo $datos2['NOM_DEPARTAMENTO']; ?></option>

        <?php } ?>
        </select>
    </td>
    </tr>
    </table>
	<td colspan="2" align="center"><button type="submit" class="btn btn-info" >Buscar</button></td>
	<br><br>
    </form>
	
	
	<form id="combo" name="combo" action="mate2.php" method="POST">
	<!--div style="width:800px; margin-left:auto; margin-right:auto;
    background-color:#78909c; background-size: cover; padding:8px;"-->
    <table aling="center" width="780" cellspacing="2" cellpadding="2" border="0">
    <tr>
    <td colspan="2" align="center" bgcolor=#4caf50><font face="verdana"color="white">CONVOCATORIAS POR MATERIA</font> </td>
    </tr>
    <tr>
    <td aling="right"> Selecciona Materia: </td>
    <td><select id="cbx_materia" name="cbx_materia">
        <option value="0">Seleccionar Materia</option>
        <?php while($datos3=mysqli_fetch_array($res3)) { ?>
          <option value="<?php echo $datos3['ID_MATERIA']; ?>"><?php echo $datos3['NOM_MATERIA']; ?></option>

        <?php } ?>
        </select>
    </td>
    </tr>
    </table>
	<td colspan="2" align="center"><button type="submit" class="btn btn-success" >Buscar</button></td>
	<br><br>
	</form>
	
	<form id="combo" name="combo" action="perio2.php" method="POST">
	<!--div style="width:800px; margin-left:auto; margin-right:auto;
    background-color:#78909c; background-size: cover; padding:8px;"-->
    <table aling="center" width="780" cellspacing="2" cellpadding="2" border="0">
    <tr>
    <td colspan="2" align="center" bgcolor=#00acc1><font face="verdana"color="white">CONVOCATORIAS POR PERIODO</font> </td>
    </tr>
    <tr>
    <td aling="right"> Selecciona Periodo: </td>
    <td><select id="cbx_periodo" name="cbx_periodo">
        <option value="0">Seleccionar Periodo</option>
        <?php while($datos4=mysqli_fetch_array($res4)) { ?>
          <option value="<?php echo $datos4['PERIODO']; ?>"><?php echo $datos4['PERIODO']; ?></option>
        <?php } ?>
        </select>
    </td>
    </tr>
    </table>
	<td colspan="2" align="center"><button type="submit" class="btn btn-info" >Buscar</button></td>
	<br><br>
	</form>
	
	</div>
    </body>
    
</html>

