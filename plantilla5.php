<html>
<?php
include("conex.php");
$link=conectar();
mysqli_set_charset($link,'utf8');
$id_convocatoria=$_REQUEST['id'];
//$id_convocatoria=20;
//Copia de seccion a seccion_e
$sql70="SELECT ID_SECCION, NOMBRE_SECCION, PUNTAJE_MAXIMO_DEL_AREA
        FROM seccion ";
$res70=mysqli_query($link, $sql70);
while($row70=mysqli_fetch_array($res70))
{
	$uno=$row70['ID_SECCION'];
	$dos=$row70['NOMBRE_SECCION'];
	$tres=$row70['PUNTAJE_MAXIMO_DEL_AREA'];
	$sql80="INSERT INTO seccion_e (ID_CONVOCATORIA,ID_SECCION,NOMBRE_SECCION,PUNTAJE_MAXIMO_DEL_AREA)
	        VALUES ('$id_convocatoria','$uno','$dos','$tres')";
	$res90=mysqli_query($link, $sql80);
	unset ($sql80,$res90);//clave
	
}
//Copia de subseccion a subseccion_e
$sql71="SELECT ID_SECCION, ID_SUBSECCION,NOM_SUBSECCION
        FROM subseccion ";
$res71=mysqli_query($link, $sql71);
while($row71=mysqli_fetch_array($res71))
{
	$uno=$row71['ID_SECCION'];
	$dos=$row71['ID_SUBSECCION'];
	$tres=$row71['NOM_SUBSECCION'];
	$sql81="INSERT INTO subseccion_e (ID_CONVOCATORIA,ID_SECCION,ID_SUBSECCION,NOM_SUBSECCION)
	        VALUES ('$id_convocatoria','$uno','$dos','$tres')";
	$res91=mysqli_query($link, $sql81);
	unset ($sql81,$res91);//clave
}


?>
<head>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.min.css" rel="stylesheet">
     <script src="dist/js/jquery-3.4.1.min.js"></script>
     <script src="dist/js/jquery-3.1.1.min.js"></script>
     <script src="dist/js/bootstrap.min.js"></script>
     <script src="dist/js/bootstrap.bundle.min.js"></script>
     <script src="dist/js/bootstrap.bundle.js"></script>
     <script src="dist/js/bootstrap.js"></script>
     <!---fontawesome 5--->
     <script src="dist/js/all.js"></script>
     <!---------->

</head>
<body>
<div class="container">
   
   <br>
   <h3><center>REGISTRO TABLA DE CALIFICACION DE MERITOS</center></h3>
   <br>
   
        
		<br>
		<?php
		      $sql99="SELECT NOMBRE_SECCION, ID_SECCION
			          FROM seccion_e
					  WHERE ID_CONVOCATORIA='$id_convocatoria'";
			  $res99=mysqli_query($link,$sql99);
		while($row99=mysqli_fetch_array($res99))
		{
		   ?>
		   <h3><?php echo $row99['NOMBRE_SECCION'];?></h3>
		   <div class="row">
	       <a href="nuevo_seccion_meritos1.php?id=<?php echo $id_convocatoria ?> &id_seccion=<?php echo $row99['ID_SECCION']; ?>" class="btn btn-info">Nuevo</a>
           </div>
		   <?php
		      $aux1=$row99['ID_SECCION'];
		      $sql100="SELECT NOM_SUBSECCION, ID_SUBSECCION
		               FROM subseccion_e
				       WHERE ID_SECCION='$aux1' and
					         ID_CONVOCATORIA='$id_convocatoria'";
		      $res100=mysqli_query($link,$sql100);
	
		   while($row100=mysqli_fetch_array($res100))
	       {
			  ?>
			   <h4><?php echo $row100['NOM_SUBSECCION']?></h4>
               <!--div class="col-md-12"-->
			   <div class="row table-responsive">
               <!--table class="table table-bordered"-->
               <table class="table table-striped">
			   
               <th>ID_CONVOCATORIA</th>
               <th>ID_SECCION</th>
               <th>ID_SUBSECCION</th>
               <th>ID_DOC</th>
               <th>NOM_DOC</th>
               <th>PUNTAJE</th>
         
			   
			   
               <?php
			     $aux2=$row100['ID_SUBSECCION'];
			     $sql101="SELECT ID_CONVOCATORIA,ID_SECCION,ID_SUBSECCION,ID_DOC,NOM_DOC,PUNTAJE
                          FROM documentacion_meritos
                          WHERE ID_SECCION='$aux1' and ID_CONVOCATORIA='$id_convocatoria' and
			                    ID_SUBSECCION='$aux2'";            
                 $res101=mysqli_query($link,$sql101);
			     while($row = mysqli_fetch_array($res101)){ ?>
       
                 <tr>
                 <td><?php echo $row['ID_CONVOCATORIA']?></td>
                 <td><?php echo $row['ID_SECCION']?></td>
                 <td><?php echo $row['ID_SUBSECCION']?></td>
                 <td><?php echo $row['ID_DOC']?></td>
                 <td><?php echo $row['NOM_DOC']?></td>  
				 <td><?php echo $row['PUNTAJE']?></td>  
				 
				  <td>
                 <a href="editar_documentacion1.php?id=<?php echo $row['ID_CONVOCATORIA'];?> &id_doc=<?php echo $row['ID_DOC'];?> &id_seccion=<?php echo $row['ID_SECCION'];?> &id_subseccion=<?php echo $row['ID_SUBSECCION'];?>" class="btn btn-secondary">
                   <i class= "fas fa-marker"></i>Editar
                 </a></td><td>
                 <a href="borrar_documentacion22.php?id=<?php echo $row['ID_CONVOCATORIA']?> &id_doc=<?php echo $row['ID_DOC'];?> &id_seccion=<?php echo $row['ID_SECCION'];?> &id_subseccion=<?php echo $row['ID_SUBSECCION'];?>" class="btn btn-danger">
                 <i class= "far fa-trash-alt"></i>Borrar
                 </a></td>
				 
				 
				 
				 
				 
			     </tr>
			   
			     <?php
			     } ?>
				 </table>
				 </div>
				 <?php
		    } 
			?>
			<BR>
			
			<?php
        }?>
	 <div class="row">
	       <a href="detalle_tabla_meritos.php?id=<?php echo $id_convocatoria ; ?>" class="btn btn-info">Continuar</a>
    </div>	
</div>


</body>
</html>