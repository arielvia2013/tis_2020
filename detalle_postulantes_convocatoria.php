<html>
<?php
include("conex.php");
$link=conectar();
mysqli_set_charset($link,'utf8');
$id=$_REQUEST['cbx_convocatoria'];

$sql1="SELECT NOM_CONVOCATORIA
       FROM convocatoria
	   WHERE COD_CONVOCATORIA='$id'";
$res1=mysqli_query($link,$sql1);
$row1=mysqli_fetch_array($res1);

$sql2="SELECT ID_POSTULANTE, NOM_POSTULANTE, APELLIDO_P, APELLIDO_M, TELEFONO, EMAIL
       FROM postulante
	   WHERE ID_POSTULANTE IN ( SELECT ID_POSTULANTE
	                            FROM inscripcion
								WHERE ID_CONVOCATORIA='$id')
	   ORDER BY APELLIDO_P ";
$res2=mysqli_query($link,$sql2);

?>
<head>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.min.css" rel="stylesheet">
     <script src="dist/js/jquery-3.4.1.min.js"></script>
     <script src="dist/js/jquery-3.1.1.min.js"></script>
     <script src="dist/js/bootstrap.min.js"></script>
     <script src="dist/js/bootstrap.bundle.min.js"></script>
     <script src="dist/js/bootstrap.bundle.js"></script>
     <script src="dist/js/bootstrap.js"></script>
     <!---fontawesome 5-->
     <script src="dist/js/all.js"></script>
     <!---------->

</head>
<body>
<div class="container">
   
   <br>
   <h3><center>TABLA DE POSTULANTES A CONVOCATORIA</center></h3>
   <br>
   
        
		<br>
		<h3>CONVOCATORIA: <?php echo $row1['NOM_CONVOCATORIA'] ?></h3>
		<!--div class="col-md-12"-->
			   <div class="row table-responsive">
               <!--table class="table table-bordered"-->
               <table class="table table-striped">
			   
               <th>APELLIDO P.</th>
			   <th>APELLIDO M.</th>
               <th>NOM_POSTULANTE</th>
               <th>TELEFONO</th>
               <th>EMAIL</th>
               <!--th>PUNTAJE</th-->
		
		
		<?php
		      
		while($row2=mysqli_fetch_array($res2))
		{
		   $aux=$row2['ID_POSTULANTE'];
		   $sql3="SELECT SUM(PUNTAJE_SISTEMA) AS PUNTAJE
		          FROM doc_postulante
				  WHERE ID_CONVOCATORIA='$id' AND
				        ID_POSTULANTE= '$aux'";
		   $res3=mysqli_query($link, $sql3);
		   $row3=mysqli_fetch_array($res3);
		   $puntaje=$row3['PUNTAJE'];
		   ?>
		     <tr>
                 <td><?php echo $row2['APELLIDO_P']?></td>
                 <td><?php echo $row2['APELLIDO_M']?></td>
                 <td><?php echo $row2['NOM_POSTULANTE']?></td>
                 <td><?php echo $row2['TELEFONO']?></td>
                 <td><?php echo $row2['EMAIL']?></td>  
				 <!--td><?php echo $puntaje?></td--> 
			     
				 
				 <td>
                 <a href="registrar_doc_postulante.php?id=<?php echo $id;?> &id_postulante=<?php echo $row2['ID_POSTULANTE'];?>" class="btn btn-success">
                   <i class= "fas fa-marker"></i>VERIFICAR CANT. DOCUMENTOS
                 </a></td><td>
                
                 
			 </tr>
	
			<?php
         } ?>
				 </table>
				 </div>
				
		 <div class="row">
	       <a href="registro_documentacion_postulante.php?id=<?php echo $id; ?>" class="btn btn-info">Continuar</a>
         </div>
</div>
</body>
</html>		
			   