<?php
  include("conex.php");
  $link=conectar();
  $consulta="SELECT ID_FACULTAD,NOM_FACULTAD
             FROM facultad";
  $res1=mysqli_query($link,$consulta);
   
 ?>
<html>
  <head>
     <title> Registro de convocatoria </title>
     <script language="javascript" src="js/jquery-3.4.1.min.js"></script>
     <script src="js/jquery-3.4.1.min.js"></script>
     <script src="js/jquery-3.1.1.min.js"></script>
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	 <link href="dist/css/bootstrap.min.css" rel="stylesheet">
	 <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
	 <script src="dist/js/jquery-3.4.1.min.js"></script>
	 <script src="dist/js/bootstrap.min.js"></script>
     <script language="javascript">
        $(document).ready(function(){
          $("#cbx_facultad").change(function(){

              $('#cbx_materia').find('option').remove().end().append(
              '<option value="whatever"></option>').val('whatever');

              $("#cbx_facultad option:selected").each(function(){
                 id_facultad=$(this).val();
                 $.post("includes/getDepartamento.php", { id_facultad: id_facultad}, function(data){
                          $("#cbx_departamento").html(data);
                 });


              });

          })
        });

         $(document).ready(function(){
          $("#cbx_departamento").change(function(){

              $("#cbx_departamento option:selected").each(function(){
                 id_departamento=$(this).val();
                 $.post("includes/getMateria.php", { id_departamento: id_departamento}, function(data){
                          $("#cbx_materia").html(data);
                 });

              });

          })
        });
     </script>
  </head>
  
<body>

    <form id="combo" name="combo" action="guardar_codigo_convocatoria.php" method="POST" enctype="multipart/form-data">
	<!--div style="width:800px; margin-left:auto; margin-right:auto;
    background-color:#78909c; background-size: cover; padding:8px;"-->
    <div style="width:800px; margin-left:auto; margin-right:auto;
    background-color:#b2dfdb; background-size: cover; padding:8px;">
    <table aling="center" width="600" cellspacing="2" cellpadding="2" border="0">
	
    <tr>
    <td colspan="2" align="center" bgcolor=#00c853><font face="verdana"color="white">REGISTRO CONVOCATORIA</font> </td>
    </tr>
    <tr>
    <td></td>
    </tr>
    <tr>
    <td aling="right"> Codigo convocatoria: </td>
    <td><input type="text" class="form-control" name="id_conv" size="40" maxlength="5" required></td>
    </tr>
    <tr>
    <td aling="right"> Nombre convocatoria: </td>
    <td><input type="text" class="form-control" name="nom_conv" size="40" maxlength="50" required></td>
    </tr>
    <tr>
    <td aling="right"> Fecha inicio: </td>
    <td><input type="date" class="form-control" name="f_inicio" size="40" maxlength="50" step="1" required></td>
    </tr>
    <tr>
    <td aling="right"> Fecha fin: </td>
    <td><input type="date" class="form-control" name="f_fin" size="40" maxlength="50" step="1" required></td>
    </tr>
    <tr>
    <td aling="right"> Fecha resultados: </td>
    <td><input type="date" class="form-control" name="f_res" size="40" maxlength="50" step="1" required></td>
    </tr>
	<tr>
	<td aling="right">Periodo: </td>
	<td><input type="text" class="form-control" name="periodo" size="40" maxlength="50" value="01-2019" required></td>
	</tr>
    <tr>
    <!--td aling="right">Puntaje total: </td>
    <td><input type="number" class="form-control" name="puntaje" size="40" maxlength="50" required></td>
    </tr-->
    <tr> <td aling="right">Pdf de la convocatoria </td>
    <td>
        <input type="file" name="archivo"></td></tr>
    <tr>
    <tr>
    <td aling="right">Codigo concurso: </td>
    <td><input type="text" class="form-control" name="cod_con" size="40" maxlength="50" required></td>
    </tr>

    <tr>
    <td aling="right"> Selecciona Facultad: </td>
    <td><select id="cbx_facultad" name="cbx_facultad">
        <option value="0">Seleccionar Facultad</option>
        <?php while($datos1=mysqli_fetch_array($res1)) { ?>
          <option value="<?php echo $datos1['ID_FACULTAD']; ?>"><?php echo $datos1['NOM_FACULTAD']; ?></option>

        <?php } ?>
        </select>
    </td>
    </tr>

    <tr>
    <td aling="right">Selecciona Departamento: </td>
    <td><select id="cbx_departamento" name="cbx_departamento">
    </select>
    </td>
    </tr>

    <tr>
    <td aling="right">Selecciona Materia: </td>
    <td><select id="cbx_materia" name="cbx_materia">
    </select>
    </td>
    </tr>
	<br>
    <td colspan="2" align="center"><button type="submit" class="btn btn-success btn-lg" >Guardar</button></td>
	<br>
    </table>
    </form>
	
	</div>
    </body>
    
</html>
