<html>
<?php
include("conex.php");
$link=conectar();
mysqli_set_charset($link,'utf8');
$consulta="SELECT ID_FACULTAD,NOM_FACULTAD
             FROM FACULTAD";
             
  $res1=mysqli_query($link,$consulta);
?>
<head>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.min.css" rel="stylesheet">
     <script src="dist/js/jquery-3.4.1.min.js"></script>
     <script src="dist/js/jquery-3.1.1.min.js"></script>
     <script src="dist/js/bootstrap.min.js"></script>
     <script src="dist/js/bootstrap.bundle.min.js"></script>
     <script src="dist/js/bootstrap.bundle.js"></script>
     <script src="dist/js/bootstrap.js"></script>
     <!---fontawesome 5--->
     <script src="dist/js/all.js"></script>
     <!---------->
<script language="javascript">
        $(document).ready(function(){
          $("#cbx_facultad").change(function(){

              $('#cbx_materia').find('option').remove().end().append(
              '<option value="whatever"></option>').val('whatever');

              $("#cbx_facultad option:selected").each(function(){
                 id_facultad=$(this).val();
                 $.post("includes/getDepartamento.php", { id_facultad: id_facultad}, function(data){
                          $("#cbx_departamento").html(data);
                 });

              });

          })
        });

         $(document).ready(function(){
          $("#cbx_departamento").change(function(){

              $("#cbx_departamento option:selected").each(function(){
                 id_departamento=$(this).val();
                 $.post("includes/getMateria.php", { id_departamento: id_departamento}, function(data){
                          $("#cbx_materia").html(data);
                 });

              });

          })
        });
     </script>
</head>
<body>
<h1>LISTADO CONVOCATORIAS VIGENTES</h1>
<div class="col-md-12">
       <table class="table table-bordered">
          <thead>
            <tr>
               <!---<th>CODIGO</th>--->
               <th>NOMBRE DE CONVOCATORIA</th>
               <th>MATERIA</th>
               <th>PDF</th>
               <th>DEPARTAMENTO</th>
               <th>FECHA INICIO</th>
               <th>FECHA FIN</th>
               <th>ACCIONES</th>
         
                           
            </tr>          
          </thead> 

          <tbody>
          <?php 
            $query = "SELECT ID_CONVOCATORIA, NOM_CONVOCATORIA, NOM_MATERIA, NOM_DEPARTAMENTO, FECHA_INICIO, FECHA_FIN, NOM_ARCH, UBI_ARCH FROM convocatoria, departamento, materia WHERE convocatoria.ID_MATERIA= materia.ID_MATERIA AND departamento.ID_DEPARTAMENTO = materia.ID_DEPARTAMENTO  ";
            $resultado_conv = mysqli_query($link,$query);
            while($row = mysqli_fetch_array($resultado_conv)){ ?>
       
              <tr>
                <td><?php echo $row['NOM_CONVOCATORIA']?></td>
                <td><?php  echo $row['NOM_MATERIA']?></td>
                <td> <?php $listar = null;
                        $nombres=['NOM_ARCH'];
                      $directorio = opendir("Convocatorias/");
                       while($elemento = readdir($directorio) ){
                          if ($elemento != '.' && $elemento != '..' && $elemento == $row['NOM_ARCH'] ){
                            if(is_dir("Convocatorias/".$elemento)){
                
                               $listar .= "<li><a href ='Convocatorias/$elemento' target='_blank' >$elemento/ </a></li>";
                            }else{
                               $listar .= "<li><a href ='Convocatorias/$elemento' target='_blank'>$elemento/ </a></li>";
            }
        
        }

    }
    echo $listar;
?></td>
                <td><?php  echo $row['NOM_DEPARTAMENTO']?></td>
                <td><?php echo $row['FECHA_INICIO']?></td>
                <td><?php echo $row['FECHA_FIN']?></td>              
                <td>
                 <a href="editar.php?ID=<?php echo $row['ID_CONVOCATORIA']?>" class="btn btn-secondary">
                   <i class= "fas fa-marker"></i>Editar
                 </a>
                 <a href="borrar_convocatoria.php?ID=<?php echo $row['ID_CONVOCATORIA']?>" class="btn btn-danger">
                 <i class= "far fa-trash-alt"></i>Borrar
                 </a>
                </td>
              </tr>

          <?php  }   ?>
          
          </tbody>
       
       </table>
       
       </div>

   
    <!---<table border="1">
    <tr>
    <th>CODIGO</th>
    <th>NOMBRE CONVOCATORIA</th>
	<th>MATERIA</th>
	<th>DEPARTAMENTO</th>
    <th>FECHA INICIO</th>
    <th>FECHA FIN</th>
    </tr>
    
    </table>--->
    

</body>
</html>








<!---<html>
  <head>
     <title> Registro de convocatoria </title>
     <script language="javascript" src="js/jquery-3.4.1.min.js"></script>
     <script src="js/jquery-3.4.1.min.js"></script>
     <script src="js/jquery-3.1.1.min.js"></script>
	 <meta name="viewport" content="width=device-width, initial-scale=1">
	 <link href="dist/css/bootstrap.min.css" rel="stylesheet">
	 <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
	 <script src="dist/js/jquery-3.4.1.min.js"></script>
	 <script src="dist/js/bootstrap.min.js"></script>
     <script language="javascript">
        $(document).ready(function(){
          $("#cbx_facultad").change(function(){

              $('#cbx_materia').find('option').remove().end().append(
              '<option value="whatever"></option>').val('whatever');

              $("#cbx_facultad option:selected").each(function(){
                 id_facultad=$(this).val();
                 $.post("includes/getDepartamento.php", { id_facultad: id_facultad}, function(data){
                          $("#cbx_departamento").html(data);
                 });

              });

          })
        });

         $(document).ready(function(){
          $("#cbx_departamento").change(function(){

              $("#cbx_departamento option:selected").each(function(){
                 id_departamento=$(this).val();
                 $.post("includes/getMateria.php", { id_departamento: id_departamento}, function(data){
                          $("#cbx_materia").html(data);
                 });

              });

          })
        });
     </script>
  </head>
  
<body>

    <form id="combo" name="combo" action="guarda.php" method="POST">
	<div style="width:800px; margin-left:auto; margin-right:auto;
    background-color:#78909c; background-size: cover; padding:8px;">

    <table aling="center" width="600" cellspacing="2" cellpadding="2" border="0">
    <tr>
    <td colspan="2" align="center" bgcolor=#26a69a><font face="verdana"color="white">REGISTRO CONVOCATORIA</font> </td>
    </tr>
    <tr>
    <td></td>
    </tr>
    <tr>
    <td aling="right"> Codigo convocatoria: </td>
    <td><input type="text" class="form-control" name="id_conv" size="40" maxlength="50" required></td>
    </tr>
    <tr>
    <td aling="right"> Nombre convocatoria: </td>
    <td><input type="text" class="form-control" name="nom_conv" size="40" maxlength="50" required></td>
    </tr>
    <tr>
    <td aling="right"> Fecha inicio: </td>
    <td><input type="date" class="form-control" name="f_inicio" size="40" maxlength="50" step="1" min="2019-10-01" max="2020-10-01" value="2019-10-01" required></td>
    </tr>
    <tr>
    <td aling="right"> Fecha fin: </td>
    <td><input type="date" class="form-control" name="f_fin" size="40" maxlength="50" step="1" min="2019-10-01" max="2020-10-01" value="2019-10-01"required></td>
    </tr>
    <tr>
    <td aling="right"> Fecha resultados: </td>
    <td><input type="date" class="form-control" name="f_res" size="40" maxlength="50" step="1" min="2019-10-01" max="2020-10-01" value="2019-10-01"required></td>
    </tr>
    <tr>
    <td aling="right">Puntaje total: </td>
    <td><input type="text" class="form-control" name="puntaje" size="40" maxlength="50" required></td>
    </tr>

    <tr>
    <td aling="right">Codigo concurso: </td>
    <td><input type="text" class="form-control" name="cod_con" size="40" maxlength="50" required></td>
    </tr>

    <tr>
    <td aling="right"> Selecciona Facultad: </td>
    <td><select id="cbx_facultad" name="cbx_facultad">
        <option value="0">Seleccionar Facultad</option>
       
        </select>
    </td>
    </tr>

    <tr>
    <td aling="right">Selecciona Departamento: </td>
    <td><select id="cbx_departamento" name="cbx_departamento">
    </select>
    </td>
    </tr>

    <tr>
    <td aling="right">Selecciona Materia: </td>
    <td><select id="cbx_materia" name="cbx_materia">
    </select>
    </td>
    </tr>
	
    </table>
    </form>
	<br>
    <td colspan="2" align="center"><button type="submit" class="btn btn-success btn-lg" >Guardar</button></td>
	</div>
    </body>
    
</html>
