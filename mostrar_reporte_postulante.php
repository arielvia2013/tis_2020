<html>
<?php
include("conex.php");
$link=conectar();
mysqli_set_charset($link,'utf8');
$id_convocatoria=$_REQUEST['cbx_convocatoria'];
$id_postulante=$_REQUEST['id_postulante'];
$id_user=$_REQUEST['id_user'];
$id=$id_convocatoria;
$aux=$id_postulante;
$sql7000="SELECT PUNTAJE_OBTENIDO
          FROM inscripcion
		  WHERE ID_POSTULANTE='$id_postulante' AND
		        ID_CONVOCATORIA='$id_convocatoria'";
$res7000=mysqli_query($link,$sql7000);
$row7000=mysqli_fetch_array($res7000);
$puntaje_postulante=$row7000['PUNTAJE_OBTENIDO'];
$sql8000="SELECT NOM_POSTULANTE, APELLIDO_P, APELLIDO_M
          FROM postulante
		  WHERE ID_POSTULANTE='$id_postulante'";
$res8000=mysqli_query($link,$sql8000);
$row8000=mysqli_fetch_array($res8000);
$sql9000="SELECT NOM_CONVOCATORIA
          FROM convocatoria
		  WHERE COD_CONVOCATORIA='$id_convocatoria'";
$res9000=mysqli_query($link,$sql9000);
$row9000=mysqli_fetch_array($res9000);
//echo $id_convocatoria;
?>
<head>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.min.css" rel="stylesheet">
     <script src="dist/js/jquery-3.4.1.min.js"></script>
     <script src="dist/js/jquery-3.1.1.min.js"></script>
     <script src="dist/js/bootstrap.min.js"></script>
     <script src="dist/js/bootstrap.bundle.min.js"></script>
     <script src="dist/js/bootstrap.bundle.js"></script>
     <script src="dist/js/bootstrap.js"></script>
     <!---fontawesome 5--->
     <script src="dist/js/all.js"></script>
     <!---------->

</head>
<body>
<div class="container">
   
   <br>
   <h3><center>REGISTRO TABLA DE CALIFICACION DE MERITOS</center></h3>
   <br>
        <h4><font color="black">CONVOCATORIA: <?php echo $row9000['NOM_CONVOCATORIA']?></font></h4>
        <h4><font color="black">POSTULANTE: <?php echo $row8000['NOM_POSTULANTE'];?> <?php echo $row8000['APELLIDO_P'];?> <?php echo $row8000['APELLIDO_M'];?></font></h4>
		<h4><font color="black">PUNTAJE FINAL CONVOCATORIA: <?php echo $puntaje_postulante ?></font></h4>
		
		<?php
		      $sql99="SELECT NOMBRE_SECCION, ID_SECCION,PUNTAJE_MAXIMO_DEL_AREA
			          FROM seccion_e
					  WHERE ID_CONVOCATORIA='$id_convocatoria'";
			  $res99=mysqli_query($link,$sql99);
	    
		while($row99=mysqli_fetch_array($res99))
		{
			//AQUI CALCULAMOS 
			  $aux_puntaje_sistema=0;
		      $aux_puntaje_comision=0;
			  $id_seccion_y=$row99['ID_SECCION'];
              $puntaje_max_seccion=$row99['PUNTAJE_MAXIMO_DEL_AREA'];
              $sql50="SELECT SUM(PUNTAJE_SISTEMA) as SUMA
			          FROM doc_postulante
					  WHERE ID_SECCION='$id_seccion_y' AND
					        ID_CONVOCATORIA='$id' AND
							ID_POSTULANTE='$aux'";
              $res50=mysqli_query($link,$sql50);
			  $row50=mysqli_fetch_array($res50);
              $aux50=$row50['SUMA'];
              if($aux50>$puntaje_max_seccion)
			  {
				  $aux50=$puntaje_max_seccion;
			  }
              $aux_puntaje_sistema=$aux_puntaje_sistema+$aux50;	  
			  unset ($sql50,$res50,$aux50);//clave
			  $sql60="SELECT *
			          FROM doc_postulante
					  WHERE ID_SECCION='$id_seccion_y'  AND
					        ID_CONVOCATORIA='$id' AND
							ID_POSTULANTE='$aux'";
			  $res60=mysqli_query($link,$sql60);
			  while($row60=mysqli_fetch_array($res60))
			  {
				$puntaje_sistema=$row60['PUNTAJE_SISTEMA'];
                $puntaje_comision=$row60['PUNTAJE_COMISION'];
                if($puntaje_comision==0)
				{
					$aux_puntaje_comision=$aux_puntaje_comision+$puntaje_sistema;
				}
				else
				{
				    $aux_puntaje_comision=$aux_puntaje_comision+$puntaje_comision;	
				}
				
			  }
			  if($aux_puntaje_comision>$puntaje_max_seccion)
              {
				  $aux_puntaje_comision=$puntaje_max_seccion;
			  }	
              unset ($sql60,$res60);//clave			
			
			
			//TERMINA EL CALCULO
		   ?>
		   <h4><font color="#00acc1"><?php echo $row99['NOMBRE_SECCION'];?></font></h4>
		   <h4><font color="#00acc1">PUNTAJE MAXIMO AREA: <?php echo $row99['PUNTAJE_MAXIMO_DEL_AREA'];?></font></h4>
		   
		   <h4><font color="#00acc1">PUNTAJE OBTENIDO AREA: <?php echo $aux_puntaje_comision;?></font></h4>
		   <br>
		   <?php
		      $aux1=$row99['ID_SECCION'];
		      $sql100="SELECT NOM_SUBSECCION, ID_SUBSECCION
		               FROM subseccion_e
				       WHERE ID_SECCION='$aux1' and
					         ID_CONVOCATORIA='$id_convocatoria'";
		      $res100=mysqli_query($link,$sql100);
	
		   while($row100=mysqli_fetch_array($res100))
	       {
			  ?>
			   <h4><font color="red"><?php echo $row100['NOM_SUBSECCION']?></font></h4>
			   
			   <br>
               <!--div class="col-md-12"-->
			   <div class="row table-responsive">
               <!--table class="table table-bordered"-->
               <table class="table table-striped">
			   
               <th>ID_CONVOCATORIA</th>
               <th>ID_SECCION</th>
               <th>ID_SUBSECCION</th>
               <th>ID_DOC</th>
               <th>NOM_DOC</th>
			   <th>PRESENTO</th>
			   <th>CANTIDAD</th>
               <th>PUNTAJE_MAXIMO</th>
               <th>PUNTAJE_SISTEMA</th>
			   <th>PUNTAJE_COMISION</th>
			    
			   
               <?php
			     $aux2=$row100['ID_SUBSECCION'];
			     $sql101="SELECT ID_CONVOCATORIA,ID_SECCION,ID_SUBSECCION,ID_DOC,NOM_DOC,PRESENTO,CANTIDAD,PUNTAJE_MAXIMO,PUNTAJE_SISTEMA,PUNTAJE_COMISION
                          FROM doc_postulante
                          WHERE ID_SECCION='$aux1' and ID_CONVOCATORIA='$id_convocatoria' and
			                    ID_SUBSECCION='$aux2' and
								ID_POSTULANTE='$id_postulante'";            
                 $res101=mysqli_query($link,$sql101);
			     while($row = mysqli_fetch_array($res101)){ ?>
       
                 <tr>
                 <td><?php echo $row['ID_CONVOCATORIA']?></td>
                 <td><?php echo $row['ID_SECCION']?></td>
                 <td><?php echo $row['ID_SUBSECCION']?></td>
                 <td><?php echo $row['ID_DOC']?></td>
                 <td><?php echo $row['NOM_DOC']?></td> 
				 <td><?php echo $row['PRESENTO']?></td>
				 <td><?php echo $row['CANTIDAD']?></td>
				 <td><?php echo $row['PUNTAJE_MAXIMO']?></td> 
                 <td><?php echo $row['PUNTAJE_SISTEMA']?></td>
                 <td><?php echo $row['PUNTAJE_COMISION']?></td>
                 
                 <td>
				 
                 <!--a href="editar_documentacion99.php?id=<?php echo $row['ID_CONVOCATORIA'];?> &id_doc=<?php echo $row['ID_DOC'];?> &id_seccion=<?php echo $row['ID_SECCION'];?> &id_subseccion=<?php echo $row['ID_SUBSECCION'];?> &id_postulante=<?php echo $id_postulante;?>" class="btn btn-secondary">
                   <i class= "fas fa-marker"></i>Registro documento
                 </a></td><td-->
                 <!--a href="califica_comision100.php?id=<?php echo $row['ID_CONVOCATORIA']?> &id_doc=<?php echo $row['ID_DOC'];?> &id_seccion=<?php echo $row['ID_SECCION'];?> &id_subseccion=<?php echo $row['ID_SUBSECCION'];?> &id_postulante=<?php echo $id_postulante;?>" class="btn btn-danger">
                 <i class= "far fa-trash-alt"></i>Califica Comision
                 </a--></td>
			     </tr>
			      
			     <?php
			     } ?>
				 </table>
				 </div>
				 <?php
				 
		    } 
			?>
			<BR>
			
			<?php
        }?>
	
    <div class="row">
	       <a href="reporte_calificaciones_postulante.php?id_user=<?php echo $id_user ?>" class="btn btn-info">Continuar</a>
    </div>	
	
</div>
</body>
</html>		
			   
			   
			   
		