<html>
<?php
include("conex.php");
$link=conectar();
mysqli_set_charset($link,'utf8');
$consulta="SELECT ID_FACULTAD,NOM_FACULTAD
             FROM facultad";             
  $res1=mysqli_query($link,$consulta);

$consulta2="SELECT NOM_DEPARTAMENTO FROM departamento, materia WHERE departamento.ID_DEPARTAMENTO = materia.ID_DEPARTAMENTO";
  $res2=mysqli_query($link,$consulta2);
?>
<head>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.min.css" rel="stylesheet">
     <script src="dist/js/jquery-3.4.1.min.js"></script>
     <script src="dist/js/jquery-3.1.1.min.js"></script>
     <script src="dist/js/bootstrap.min.js"></script>
     <script src="dist/js/bootstrap.bundle.min.js"></script>
     <script src="dist/js/bootstrap.bundle.js"></script>
     <script src="dist/js/bootstrap.js"></script>
<script language="javascript">
        $(document).ready(function(){
          $("#cbx_facultad").change(function(){

              $('#cbx_materia').find('option').remove().end().append(
              '<option value="whatever"></option>').val('whatever');

              $("#cbx_facultad option:selected").each(function(){
                 id_facultad=$(this).val();
                 $.post("includes/getDepartamento.php", { id_facultad: id_facultad}, function(data){
                          $("#cbx_departamento").html(data);
                 });

              });

          })
        });

         $(document).ready(function(){
          $("#cbx_departamento").change(function(){

              $("#cbx_departamento option:selected").each(function(){
                 id_departamento=$(this).val();
                 $.post("includes/getMateria.php", { id_departamento: id_departamento}, function(data){
                          $("#cbx_materia").html(data);
                 });

              });

          })
        });
     </script>
</head>
<body>
<h1>LISTADO CONVOCATORIAS VIGENTES</h1>
<div class="col-md-12">
       <table class="table table-bordered">
          <thead>
            <tr>
               <!---<th>CODIGO</th>--->
               <th>NOMBRE DE CONVOCATORIA</th>
               <th>MATERIA</th>
               <th>DEPARTAMENTO</th>
               <th>FECHA INICIO</th>
               <th>FECHA FIN</th>
               <th>ACCIONES</th>        
                           
            </tr>          
          </thead> 

          <tbody>
          <?php 
            $query = "SELECT COD_CONVOCATORIA, NOM_CONVOCATORIA, NOM_MATERIA, NOM_DEPARTAMENTO, FECHA_INICIO, FECHA_FIN FROM convocatoria, departamento, materia WHERE convocatoria.ID_MATERIA= materia.ID_MATERIA AND departamento.ID_DEPARTAMENTO = materia.ID_DEPARTAMENTO  ";
            $resultado_conv = mysqli_query($link,$query);
            while($row = mysqli_fetch_array($resultado_conv)){ ?>
       
              <tr>
                <td><?php echo $row['NOM_CONVOCATORIA']?></td>
                <td><?php  echo $row['NOM_MATERIA']?></td>
                <td><?php  echo $row['NOM_DEPARTAMENTO']?></td>
                <td><?php echo $row['FECHA_INICIO']?></td>
                <td><?php echo $row['FECHA_FIN']?></td>
                <td>
                 
                 <a href="listar_postulantesH.php?ID=<?php echo $row['COD_CONVOCATORIA']?>" class="btn btn-danger">
                 <i class= "far fa-trash-alt"></i>Ver postulantes
                 </a>
                </td>
              </tr>

          <?php  }   ?>
          
          </tbody>
       
       </table>
       
       </div>

   
    <!---<table border="1">
    <tr>
    <th>CODIGO</th>
    <th>NOMBRE CONVOCATORIA</th>
	<th>MATERIA</th>
	<th>DEPARTAMENTO</th>
    <th>FECHA INICIO</th>
    <th>FECHA FIN</th>
    </tr>
    
    </table>--->
    

</body>
</html>