<html>
<?php
include("conex.php");
$link=conectar();
mysqli_set_charset($link,'utf8');
$id_convocatoria=$_REQUEST['id'];//OJO VOLVER A PONER
//$id_convocatoria=4;//Y ESTE QUITAR
$consulta1="SELECT ID_CONVOCATORIA,TIPO_REQUISITO,ID_REQUISITO,NOM_REQUISITO
            FROM requisitos 
            WHERE ID_CONVOCATORIA='$id_convocatoria' AND 
			      TIPO_REQUISITO='2' AND
				  HABILITADO='1'";
$res1=mysqli_query($link,$consulta1);
$consulta2="SELECT COUNT(*) as cantidad
            FROM requisitos
			WHERE ID_CONVOCATORIA='$id_convocatoria' AND 
			      TIPO_REQUISITO=2 ";
$res2=mysqli_query($link,$consulta2);
$row=mysqli_fetch_array($res2);
$cantidad=$row['cantidad'];
 
?>
<head>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.min.css" rel="stylesheet">
     <script src="dist/js/jquery-3.4.1.min.js"></script>
     <script src="dist/js/jquery-3.1.1.min.js"></script>
     <script src="dist/js/bootstrap.min.js"></script>
     <script src="dist/js/bootstrap.bundle.min.js"></script>
     <script src="dist/js/bootstrap.bundle.js"></script>
     <script src="dist/js/bootstrap.js"></script>
     <!---fontawesome 5--->
     <script src="dist/js/all.js"></script>
     <!---------->

</head>
<body>
   
   
   <div class="container">
        <h4>TERCER PASO</h4>
        <h4>REGISTRO REQUISITOS GENERALES</h4>
        <br>
		<div class="container">
	    <a href="nuevo_req_general.php?id=<?php echo $id_convocatoria ?>" class="btn btn-info"> + Nuevo Requisito General</a>
        </div>
   
   
        
		<br>
        <div class="col-md-12">
          <table class="table table-bordered">
            <thead>
               <tr>
               <!---<th>CODIGO</th>--->
               <th>ID_CONVOCATORIA</th>
               <th>TIPO_REQUISITO</th>
               <th>ID_REQUISITO</th>
               <th>NOM_REQUISITO</th>
             
         
                           
               </tr>          
            </thead> 

          <tbody>
          <?php 
            
            while($row = mysqli_fetch_array($res1)){ ?>
       
              <tr>
                <td><?php echo $row['ID_CONVOCATORIA']?></td>
                <td><?php echo $row['TIPO_REQUISITO']?></td>
                <td><?php echo $row['ID_REQUISITO']?></td>
                <td><?php echo $row['NOM_REQUISITO']?></td>
                      
                <td>
                 <a href="editar_doc_general.php?id=<?php echo $id_convocatoria;?> &id_requisito=<?php echo $row['ID_REQUISITO'];?> &tipo_requisito=<?php echo $row['TIPO_REQUISITO'];?>" class="btn btn-secondary">
                   <i class= "fas fa-marker"></i>Editar
                 </a>
                 <a href="borrar_doc_general.php?id=<?php echo $id_convocatoria;?> &id_requisito=<?php echo $row['ID_REQUISITO'];?> &tipo_requisito=<?php echo $row['TIPO_REQUISITO'];?>" class="btn btn-danger">
                 <i class= "far fa-trash-alt"></i>Borrar
                 </a>
                </td>
              </tr>

          <?php  }   ?>
          
          </tbody>
       
       </table>
       
       </div>

   </div>
   <BR><BR>
   <div class="container">
   <?php
      if($cantidad>0)
	  {?>
		 
		 <a href="escoger_plantilla.php?id=<?php echo $id_convocatoria;?> " base target="derecha" class="nav-link"><button class="btn btn-info">SIGUIENTE</button></a>
	     <a href="crear_tabla_meritos.php?id=<?php echo $id_convocatoria;?> " base target="derecha" class="nav-link"><button class="btn btn-secondary">CANCELAR</button></a>
	   <?php
	  }
   ?>
   </div>
   
   
</body>
</html>

