<html>
<?php
include("conex.php");
$link=conectar();
mysqli_set_charset($link,'utf8');
$id=$_REQUEST['cbx_convocatoria'];
//$id=4;
//Lo primero sera registrar nota final de postulantes a convocatoria
//en la tabla inscripcion
$sql40="UPDATE convocatoria
        SET CALIFICADO='1'
		WHERE COD_CONVOCATORIA='$id'";
$res40=mysqli_query($link,$sql40);

$sql1000="SELECT NOM_CONVOCATORIA,FECHA_RESULTADO
          FROM convocatoria
		  WHERE COD_CONVOCATORIA='$id'";
$res1000=mysqli_query($link,$sql1000);
$row1000=mysqli_fetch_array($res1000);

$sql1="SELECT *
       FROM inscripcion
	   WHERE ID_CONVOCATORIA='$id'";
$res1=mysqli_query($link,$sql1);
while($row1=mysqli_fetch_array($res1))
{
           $aux=$row1['ID_POSTULANTE'];
           $sql40="SELECT ID_SECCION,PUNTAJE_MAXIMO_DEL_AREA
		           FROM seccion_e
				   WHERE ID_CONVOCATORIA='$id'";
		   $res40=mysqli_query($link,$sql40);
		   $aux_puntaje_sistema=0;
		   $aux_puntaje_comision=0;
		   while($row40=mysqli_fetch_array($res40))
		   {
			  $id_seccion_y=$row40['ID_SECCION'];
              $puntaje_max_seccion=$row40['PUNTAJE_MAXIMO_DEL_AREA'];
              $sql50="SELECT SUM(PUNTAJE_SISTEMA) as SUMA
			          FROM doc_postulante
					  WHERE ID_SECCION='$id_seccion_y' AND
					        ID_CONVOCATORIA='$id' AND
							ID_POSTULANTE='$aux'";
              $res50=mysqli_query($link,$sql50);
			  $row50=mysqli_fetch_array($res50);
              $aux50=$row50['SUMA'];
              if($aux50>$puntaje_max_seccion)
			  {
				  $aux50=$puntaje_max_seccion;
			  }
              $aux_puntaje_sistema=$aux_puntaje_sistema+$aux50;	  
			  unset ($sql50,$res50,$aux50);//clave
			  $sql60="SELECT *
			          FROM doc_postulante
					  WHERE ID_SECCION='$id_seccion_y'  AND
					        ID_CONVOCATORIA='$id' AND
							ID_POSTULANTE='$aux'";
			  $res60=mysqli_query($link,$sql60);
			  while($row60=mysqli_fetch_array($res60))
			  {
				$puntaje_sistema=$row60['PUNTAJE_SISTEMA'];
                $puntaje_comision=$row60['PUNTAJE_COMISION'];
                if($puntaje_comision==0)
				{
					$aux_puntaje_comision=$aux_puntaje_comision+$puntaje_sistema;
				}
				else
				{
				    $aux_puntaje_comision=$aux_puntaje_comision+$puntaje_comision;	
				}
				
			  }
			  if($aux_puntaje_comision>$puntaje_max_seccion)
              {
				  $aux_puntaje_comision=$puntaje_max_seccion;
			  }	
              unset ($sql60,$res60);//clave			  
		   }
		   $sql700="UPDATE inscripcion
		            SET PUNTAJE_OBTENIDO='$aux_puntaje_comision'
					WHERE ID_POSTULANTE='$aux' AND
					      ID_CONVOCATORIA='$id'";
		   $res700=mysqli_query($link,$sql700);
		   unset ($sql700,$res700);
		   
}	

?>
<head>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.min.css" rel="stylesheet">
     <script src="dist/js/jquery-3.4.1.min.js"></script>
     <script src="dist/js/jquery-3.1.1.min.js"></script>
     <script src="dist/js/bootstrap.min.js"></script>
     <script src="dist/js/bootstrap.bundle.min.js"></script>
     <script src="dist/js/bootstrap.bundle.js"></script>
     <script src="dist/js/bootstrap.js"></script>
     <!---fontawesome 5-->
     <script src="dist/js/all.js"></script>
     <!---------->

</head>
<body>
<div class="container">
   
   <br>
   
   <h3><center>TABLA DE POSTULANTES A CONVOCATORIA</center></h3><br>
   <h3><center>CALIFICACIONES FINALES</center></h3>
        
		<br>
		<h3>CONVOCATORIA: <?php echo $row1000['NOM_CONVOCATORIA'] ?></h3>
		<h3>FECHA DE RESULTADO: <?php echo $row1000['FECHA_RESULTADO'] ?></h3>
		<!--div class="col-md-12"-->
			   <div class="row table-responsive">
               <!--table class="table table-bordered"-->
               <table class="table table-striped">
			   
               <th>APELLIDO P.</th>
			   <th>APELLIDO M.</th>
               <th>NOM_POSTULANTE</th>
		       <th>PUNTAJE_OBTENIDO</th>
		
		<?php
		 $sql1000="SELECT *
                   FROM inscripcion
                   WHERE ID_CONVOCATORIA='$id'
				   ORDER BY PUNTAJE_OBTENIDO DESC";
         $res1000=mysqli_query($link,$sql1000);	
          while($row1000=mysqli_fetch_array($res1000))
          {	
            $id_postulante=$row1000['ID_POSTULANTE'];
			$puntaje_obtenido=$row1000['PUNTAJE_OBTENIDO'];
            $sql2000="SELECT *
                      FROM postulante
					  WHERE ID_POSTULANTE='$id_postulante'";
			$res2000=mysqli_query($link,$sql2000);
			$row2000=mysqli_fetch_array($res2000);
			$ap_p=$row2000['APELLIDO_P'];
		    $ap_m=$row2000['APELLIDO_M'];
			$nombre=$row2000['NOM_POSTULANTE'];
		   ?>
		     <tr>
                 <td><?php echo $ap_p ?></td>
                 <td><?php echo $ap_m ?></td>
                 <td><?php echo $nombre?></td>
                 <td><?php echo $puntaje_obtenido?></td>
                 
				 
				 <!--td>
                 <a href="registrar_doc_postulante_2.php?id=<?php echo $id;?> &id_postulante=<?php echo $row2['ID_POSTULANTE'];?>" class="btn btn-success">
                   <i class= "fas fa-marker"></i>CALIFICAR DOCUMENTOS
                 </a>
				 </td-->
                
                 
			 </tr>
	
			  <?php
           } ?>
				 </table>
				 </div>
		 <br>
		 <div class="row">
	       <a href="resultados_convocatorias_seleccion.php" class="btn btn-info">Continuar</a>
         </div>
</div>
</body>
</html>		