<html>
<?php
include("conex.php");
$link=conectar();
mysqli_set_charset($link,'utf8');
$consulta="SELECT ID_FACULTAD,NOM_FACULTAD
             FROM facultad";             
  $res1=mysqli_query($link,$consulta);

$consulta2="SELECT NOM_DEPARTAMENTO FROM departamento, materia WHERE departamento.ID_DEPARTAMENTO = materia.ID_DEPARTAMENTO";
  $res2=mysqli_query($link,$consulta2);
?>
<head>
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.css" rel="stylesheet">
     <link href="dist/css/bootstrap-grid.min.css" rel="stylesheet">
     <link href="dist/css/bootstrap-reboot.min.css" rel="stylesheet">
     <script src="dist/js/jquery-3.4.1.min.js"></script>
     <script src="dist/js/jquery-3.1.1.min.js"></script>
     <script src="dist/js/bootstrap.min.js"></script>
     <script src="dist/js/bootstrap.bundle.min.js"></script>
     <script src="dist/js/bootstrap.bundle.js"></script>
     <script src="dist/js/bootstrap.js"></script>
<script language="javascript">
        $(document).ready(function(){
          $("#cbx_facultad").change(function(){

              $('#cbx_materia').find('option').remove().end().append(
              '<option value="whatever"></option>').val('whatever');

              $("#cbx_facultad option:selected").each(function(){
                 id_facultad=$(this).val();
                 $.post("includes/getDepartamento.php", { id_facultad: id_facultad}, function(data){
                          $("#cbx_departamento").html(data);
                 });

              });

          })
        });

         $(document).ready(function(){
          $("#cbx_departamento").change(function(){

              $("#cbx_departamento option:selected").each(function(){
                 id_departamento=$(this).val();
                 $.post("includes/getMateria.php", { id_departamento: id_departamento}, function(data){
                          $("#cbx_materia").html(data);
                 });

              });

          })
        });
     </script>
</head>
<body>
<h1>LISTADO CONVOCATORIAS VIGENTES</h1>
<div class="col-md-12">
       <table class="table table-bordered">
          <thead>
            <tr>
               <!---<th>CODIGO</th>--->
               <th>NOMBRE DE CONVOCATORIA</th>
               <th>MATERIA</th>
               <th>DEPARTAMENTO</th>
               <th>ACCIONES</th>        
                           
            </tr>          
          </thead> 

          <tbody>
          <?php 
            //if(isset($_GET['id_user'])){
                //$id= $_GET['id_user']; 
				$id=$_REQUEST['id_user']; //OJO IMPORTANTE
				//$id=23;
             // echo $id;
            /*$query = "SELECT ID_POSTULANTE, NOM_CONVOCATORIA, NOM_MATERIA, 
            NOM_DEPARTAMENTO             
            FROM convocatoria, departamento, materia, habilitado, postulante, usuario
            WHERE convocatoria.ID_MATERIA= materia.ID_MATERIA 
            AND departamento.ID_DEPARTAMENTO = materia.ID_DEPARTAMENTO 
            AND habilitado.COD_CONVOCATORIA=convocatoria.COD_CONVOCATORIA             
            AND habilitado.CI_HABILITADO=postulante.CI_POSTULANTE
            AND postulante.ID_USER=usuario.ID_USER
             AND usuario.ID_USER= '$id'";*/
             $sql="SELECT ID_POSTULANTE
                   FROM postulante
		           WHERE ID_USER='$id'";
            $res99=mysqli_query($link, $sql);
            $row99=mysqli_fetch_array($res99);
            $id_postulante=$row99['ID_POSTULANTE'];
  
            /*$query="SELECT COD_CONVOCATORIA, NOM_CONVOCATORIA, NOM_MATERIA, NOM_DEPARTAMENTO
                       FROM convocatoria,inscripcion,materia,departamento
			           WHERE inscripcion.ID_POSTULANTE='$id_postulante' AND
			          inscripcion.ID_CONVOCATORIA=convocatoria.COD_CONVOCATORIA AND
					  convocatoria.ID_MATERIA=materia.ID_MATERIA AND
					  materia.ID_DEPARTAMENTO=departamento.ID_DEPARTAMENTO";*/
		    //echo $id_postulante;	  
			$query="SELECT convocatoria.COD_CONVOCATORIA, convocatoria.NOM_CONVOCATORIA,materia.NOM_MATERIA,departamento.NOM_DEPARTAMENTO
			        FROM convocatoria, materia,departamento
					WHERE COD_CONVOCATORIA IN (SELECT ID_CONVOCATORIA
					                           FROM inscripcion
											   WHERE ID_POSTULANTE='$id_postulante')
						  AND convocatoria.ID_MATERIA=materia.ID_MATERIA 
						  AND materia.ID_DEPARTAMENTO=departamento.ID_DEPARTAMENTO";		  
            //$query=mysqli_query($link,$consulta);
            

            $resultado_conv = mysqli_query($link,$query);
			//if($que=mysqli_num_rows($resultado_conv)>0)
			//{
            while($row = mysqli_fetch_array($resultado_conv))
            { 
            
               $id_convocatoria=$row['COD_CONVOCATORIA'];
			   ?>
             <tr>
                <td><?php echo $row['NOM_CONVOCATORIA']?></td>
                <td><?php  echo $row['NOM_MATERIA']?></td>
                <td><?php  echo $row['NOM_DEPARTAMENTO']?></td>
               
                <td><a href="registrar_doc_postulante1.php?id_postulante=<?php echo $id_postulante ;?>&id_convocatoria=<?php echo $id_convocatoria ?>&id_user=<?php echo $id ?>&nom_conv=<?php echo $row['NOM_CONVOCATORIA'];?>" class="btn btn-secondary">
                   <i class= "fas fa-marker"></i>Subir documentos</a></td>
                <!--td><a href="subir_documentacion_diego.php?id_user=<?php echo $id ;?>" class="btn btn-secondary">
                   <i class= "fas fa-marker"></i>Subir documentos</a></td-->
              </tr>

             <?php  
		    }
           // }			
			//} ?>
          
          </tbody>
       
       </table>
       
       </div>